<?php

namespace Drupal\custom_migrate\Plugin\migrate\destination;

//use Drupal\migrate\Row;
//use Drupal\migrate\MigrateException;
//use Drupal\file\Plugin\migrate\destination\EntityFile as EntityFile;

use Drupal\Core\Entity\Entity;
use Drupal\migrate\Plugin\migrate\destination\EntityContentBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Plugin\migrate\destination\EntityFile as EntityFile;
use Drupal\migrate\Plugin\MigrateIdMapInterface;
use Drupal\node\Entity\Node;

/**
 * @MigrateDestination(
 *   id = "entity_file_custom_field_title_image"
 * )
 */
class EntityFileCustomFieldTitleImage extends EntityFile {
  /** @var string $entityType */
  public static $entityType = 'file';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return parent::create($container, $configuration, 'entity:' . static::$entityType, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $this->rollbackAction = MigrateIdMapInterface::ROLLBACK_DELETE;
    $entity = $this->getEntity($row, $old_destination_id_values);
    if (!$entity) {
      throw new MigrateException('Unable to get entity');
    }

    $ids = $this->save($entity, $old_destination_id_values);
    if (!empty($this->configuration['translations'])) {
      $ids[] = $entity->language()->getId();
    }

    $id = reset($ids);
    $nid = $row->getSourceProperty("nid");
    $node = Node::load($nid);
    if ($node !== NULL) {
      //var_dump($id);
      $node->field_title_image->set($id);
      $node->save();
    }

    return $ids;
  }
}
